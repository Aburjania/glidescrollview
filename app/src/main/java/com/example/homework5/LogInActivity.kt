package com.example.homework5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_log_in.*

class LogInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        init()
    }

    private fun init() {
        logInButton.setOnClickListener() {
            logIn()
        }
    }

    private fun logIn() {

        if (emailEditText.text.toString().isEmpty()) {
            emailEditText.error = "Please enter Email!"
            emailEditText.requestFocus()

        }

        if (passwordEditText.text.toString().isEmpty()) {
            passwordEditText.error = "Please enter Password!"
            passwordEditText.requestFocus()
        }

        if (emailEditText.text.toString().isNotEmpty() && passwordEditText.text.toString()
                .isNotEmpty()
        ) {
            ProfilePage()
        }
    }


    private fun ProfilePage() {
        val intent = Intent(this, ProfilePageActivity::class.java)
        startActivity(intent)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

    }
}


